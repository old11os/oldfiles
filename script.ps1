Write-Host "Installing Old11 v2!" -ForegroundColor Green
Write-Host "This may take a while..." -ForegroundColor Yellow

# Define base and log directories
$baseDir = Join-Path ([System.IO.Path]::GetTempPath()) "oldfiles-main\oldfiles-main"
$logDir = Join-Path $baseDir "logs"
$logFile = Join-Path $logDir "install_log.txt"

# Create log directory and log file
if (-not (Test-Path $logDir)) {
    New-Item -Path $logDir -ItemType Directory
}
New-Item -Path $logFile -ItemType File -Force

# Function to log messages
function Log-Message {
    param (
        [string]$message
    )
    $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    "$timestamp - $message" | Out-File -FilePath $logFile -Append
}

# Stop explorer.exe and rundll32.exe processes
Stop-Process -Name explorer -Force
Log-Message "Stopped explorer.exe process."

# Install patcher
Start-Process -FilePath (Join-Path $baseDir "patcher.exe") -Wait
Log-Message "Ran patcher.exe."

Start-Process -FilePath "regedit.exe" -ArgumentList "/s `"$baseDir\dockpatcher.reg`"" -Wait
Log-Message "Applied registry patch."

Stop-Process -Name explorer -Force
Log-Message "Stopped explorer.exe process again."

# Function to copy and replace directories
function Copy-ReplaceDirectory {
    param (
        [string]$source,
        [string]$destination
    )
    if (Test-Path $source) {
        if (Test-Path $destination) {
            Remove-Item -Path $destination -Recurse -Force
            Log-Message "Deleted existing directory: $destination"
        }
        Copy-Item -Path $source -Destination $destination -Recurse -Force
        Log-Message "Copied directory from $source to $destination"
    } else {
        Log-Message "Source directory not found: $source"
    }
}

# Function to copy and replace files
function Copy-ReplaceFile {
    param (
        [string]$source,
        [string]$destination
    )
    if (Test-Path $source) {
        Copy-Item -Path $source -Destination $destination -Force
        Log-Message "Copied file from $source to $destination"
    } else {
        Log-Message "Source file not found: $source"
    }
}

# Patch Settings app
Copy-ReplaceDirectory (Join-Path $baseDir "Windows.UI.SettingsAppThreshold") "C:\Windows\SystemResources\Windows.UI.SettingsAppThreshold"

# Replace ImmersiveControlPanel directory
Copy-ReplaceDirectory (Join-Path $baseDir "ImmersiveControlPanel") "C:\Windows\ImmersiveControlPanel"

# Replace SettingsEnvironment.Desktop.dll in C:\Windows\System32
Copy-ReplaceFile (Join-Path $baseDir "SettingsEnvironment.Desktop.dll") "C:\Windows\System32\SettingsEnvironment.Desktop.dll"

# Patch Icons in C:\Windows\SystemResources
$munFiles = @("imageres.dll.mun", "DDORes.dll.mun", "shell32.dll.mun")
foreach ($munFile in $munFiles) {
    Copy-ReplaceFile (Join-Path $baseDir $munFile) (Join-Path "C:\Windows\SystemResources" $munFile)
}

# Patch Bootscreen in C:\Windows\Boot\Resources
Copy-ReplaceFile (Join-Path $baseDir "bootres.dll") "C:\Windows\Boot\Resources\bootres.dll"

# Patch Branding in C:\Windows\Branding\Basebrd
Copy-ReplaceFile (Join-Path $baseDir "basebrd.dll") "C:\Windows\Branding\Basebrd\basebrd.dll"

# Patch Action Center in C:\Windows\SystemResources\Windows.UI.ShellCommon
Copy-ReplaceDirectory (Join-Path $baseDir "Windows.UI.ShellCommon") "C:\Windows\SystemResources\Windows.UI.ShellCommon"

# Patch twinui.dll and related files in C:\Windows\System32
$filesToReplace = @(
    "twinui.dll",
    "twinui.appcore.dll",
    "twinui.pcshell.dll",
    "Windows.UI.FileExplorer.dll",
    "SettingsHandlers_nt.dll",
    "SettingsHandlers_DesktopTaskbar.dll",
    "dwmapi.dll",
    "LogonController.dll"
)
foreach ($file in $filesToReplace) {
    Copy-ReplaceFile (Join-Path $baseDir $file) (Join-Path "C:\Windows\System32" $file)
}

# Patch Windows Search in C:\Windows\SystemApps
Copy-ReplaceDirectory (Join-Path $baseDir "Microsoft.Windows.Search_cw5n1h2txyewy") "C:\Windows\SystemApps\Microsoft.Windows.Search_cw5n1h2txyewy"

# Patch Sounds in C:\Windows\Media
Copy-ReplaceDirectory (Join-Path $baseDir "Media") "C:\Windows\Media"

# Restart the computer to complete installation
Log-Message "Restarting the computer to complete installation."
Restart-Computer -Force
